defmodule Bakery do
  alias Bakery.{Bowl, Oven, Pan}
  import GenServer, only: [call: 2]
  import Bakery.Oven, only: [heat_to: 2, wait: 2, turn_off: 1]

  def bake_cake do
    oven = Oven.turn_on |> heat_to(175)

    pan = Pan.take_by_type({:rounded, 7})
    |> call(:grease)
    |> call(:flour)

    small_bowl = Bowl.take_by_size(:small)
    |> call({:put, :flour})
    |> call({:put, :baking_soda})
    |> call({:put, :salt})
    |> call(:whisk_together)

    large_bowl = Bowl.take_by_size(:large)
    |> call({:put, :butter})
    |> call({:put, :white_sugar})
    |> call({:put, :brown_sugar})
    |> call({:cream, until: :light_and_fluffy})
    |> call({:beat_in, :egg})
    |> call({:beat_in, :egg})
    |> call(:mix_up)
    |> call({:put, :bananas})
    |> call({:put, call(small_bowl, :contents)})
    |> call({:put, :kefir})
    |> call(:mix_up)
    |> call({:put, call(small_bowl, :contents)})
    |> call({:put, :kefir})
    |> call({:put, :chopped_walnuts})
    |> call(:mix_up)

    pan
    |> call({:get_contents_from, large_bowl})
    |> call({:set_location, oven})

    oven
    |> wait({30, :minutes})
    |> turn_off

    pan |> call({:set_location, :damp_tea_towel})

    :ok
  end
end
