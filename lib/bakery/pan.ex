defmodule Bakery.Pan do
  use GenServer

  # API

  def take_by_type(type) do
    {:ok, pid} = GenServer.start_link(__MODULE__, %{type: type, greased: false, floured: false, location: nil, contents: []}, name: __MODULE__)
    pid
  end

  # Callbacks

  def handle_call(:grease, _from, state) do
    {:reply, __MODULE__, Map.put(state, :greased, true)}
  end

  def handle_call(:flour, _from, state) do
    {:reply, __MODULE__, Map.put(state, :floured, true)}
  end

  def handle_call({:set_location, location}, _from, state) do
    {:reply, __MODULE__, Map.put(state, :location, location)}
  end

  def handle_call({:get_contents_from, bowl}, _from, state) do
    contents = GenServer.call(bowl, :contents)
    {:reply, __MODULE__, Map.put(state, :contents, contents)}
  end
end
