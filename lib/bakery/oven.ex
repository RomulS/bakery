defmodule Bakery.Oven do
  use GenServer

  # API

  def turn_on do
    {:ok, pid} = GenServer.start_link(__MODULE__, %{temperature: 0, content: nil}, name: __MODULE__)
    pid
  end

  def heat_to(pid, temperature) do
    GenServer.call(pid, {:heat_to, temperature})
  end

  def wait(pid, {minutes, :minutes}) do
    GenServer.call(pid, {:wait, minutes})
  end

  def turn_off(pid) do
    GenServer.stop(pid)
    pid
  end

  # Callbacks

  def handle_call({:heat_to, temperature}, _from, state) do
    {:reply, __MODULE__, Map.put(state, :temperature, temperature)}
  end

  def handle_call({:wait, minutes}, _from, state) do
    # :timer.sleep(minutes * 60_000)
    {:reply, __MODULE__, state}
  end
end
