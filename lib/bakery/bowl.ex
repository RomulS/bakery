defmodule Bakery.Bowl do
  use GenServer

  # API

  def take_by_size(size) do
    {:ok, pid} = GenServer.start_link(__MODULE__, %{size: size, contents: [], condition: nil})
    pid
  end

  # Callbacks

  def handle_call({:put, ingredient}, _from, state) do
    {:reply, self, Map.put(state, :contents, [ingredient | state[:contents]])}
  end

  def handle_call(action, _from, state) when action in [:whisk_together, :mix_up] do
    condition = to_string(action) |> String.replace("_", "ed_") |> String.to_atom
    {:reply, self, Map.put(state, :condition, condition)}
  end

  def handle_call({:cream, until: desired_condition}, _from, state) do
    new_condition = cream(state[:condition], desired_condition, 5)
    {:reply, self, Map.put(state, :condition, new_condition)}
  end

  def handle_call({:beat_in, ingredient}, from, state) do
    handle_call({:put, ingredient}, from, state)
  end

  def handle_call(:contents, _from, state) do
    {:reply, state[:contents], state}
  end

  # Private functions

  defp cream(_, desired_condition, 0), do: desired_condition
  defp cream(desired_condition, desired_condition, _), do: desired_condition
  defp cream(condition, desired_condition, count) do
    new_condition = case condition do
      nil -> :light
      :light -> :light_and_fluffy
      _ -> nil
    end
    cream(new_condition, desired_condition, count-1)
  end

end
